import { Injectable } from '@angular/core';
import { switchMap } from 'rxjs/operators';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { CourseService } from 'src/app/services/course.service';
import { loadCourse, loadCourseSuccess, loadCourseFailure } from '../actions/course.actions';


@Injectable()
export class CourseEffects {

  constructor(
    private actions$: Actions,
    private courseService: CourseService
  ) {}

  @Effect()
  loadCource$ = this.actions$.pipe(
    ofType(loadCourse),
    switchMap(async () => {
      const cource: number | {message: string} = await this.courseService.getCourseData();

      return typeof cource === 'number'
        ? loadCourseSuccess({value: cource})
        : loadCourseFailure({error: cource.message });
    })
  );
}
