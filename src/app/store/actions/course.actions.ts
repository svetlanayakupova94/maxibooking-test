import { createAction, props } from '@ngrx/store';


export const loadCourse = createAction(
  '[Course] Load Course'
);

export const loadCourseSuccess = createAction(
  '[Course] Load Course Success',
  props<{ value: number }>()
);

export const loadCourseFailure = createAction(
  '[Course] Load Course Failure',
  props<{ error: string }>()
);
