import { Action, createReducer, on } from '@ngrx/store';
import { loadCourseFailure, loadCourseSuccess } from '../actions/course.actions';


export const courseFeatureKey = 'course';

export interface State {
  value: number;
  error: string;
}

export const initialState: State = {
  value: 0,
  error: ''
};

const courseReducer = createReducer(
  initialState,
  on(loadCourseSuccess, (state, action) => ({...state, value: action.value })),
  on(loadCourseFailure, (state, action) => ({...state, error: action.error })),
);

export function reducer(state: State | undefined, action: Action) {
  return courseReducer(state, action);
}
