import { ActionReducerMap } from '@ngrx/store';
import { AppState } from '../interfaces/app-state';
import { reducer as courseReducer } from './course.reducer';


export const reducers: ActionReducerMap<AppState> = {
  course: courseReducer
};
