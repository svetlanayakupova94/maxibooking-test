import { courseFeatureKey, State as CourseState } from '../reducers/course.reducer';


export interface AppState {
  [courseFeatureKey]: CourseState;
}
