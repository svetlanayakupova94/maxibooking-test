import { CurrencyXML } from './currency-xml';
import { CurrencyJSON } from './currency-json';


export type SourceInstances = CurrencyXML | CurrencyJSON;

export enum Currencies {
  EURO = 'EUR'
}
