import { SourceInstances } from './sources';
import { CurrencyXML } from './currency-xml';
import { CurrencyJSON } from './currency-json';
import { HttpClient } from '@angular/common/http';


export enum SourceList {
  CBR_XML = 'cbr_xml',
  CBR_JSON = 'cbr_json',
}

export class CurrencyFactory {
  create(url: string, type: SourceList, http: HttpClient): SourceInstances {
    switch (type) {
      case SourceList.CBR_JSON:
        return new CurrencyJSON(url, http);
      case SourceList.CBR_XML:
        return  new CurrencyXML(url, http);
      default:
        return null;
    }
  }
}
