import * as xml2js from 'xml2js';
import { Observable, of } from 'rxjs';
import { Currencies } from './sources';
import { catchError, map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { CurrencyParrent } from '../interfaces/currency-parrent';
import { CurrencyDailyXML } from '../interfaces/currency-daily-xml';
import { Parser } from 'xml2js';


export class CurrencyXML implements CurrencyParrent {

  constructor(
    private url: string,
    private http: HttpClient
  ) {}

  getCourse(): Observable<number> {
    return this.http.get(this.url, {responseType: 'text'}).pipe(
      map((res) => {
        let parsedResult: CurrencyDailyXML;
        const parser: Parser = new xml2js.Parser({ strict: false, trim: true });

        parser.parseString(res, (err, result) => parsedResult = result);

        return parsedResult;
      }),
      catchError((error) => {
        console.log('error', error);
        return of(null);
      }),
      map((res: CurrencyDailyXML) => {
        if (res && res.VALCURS) {
          return this.convertStrToNum(
            res.VALCURS.VALUTE.find(val => val.CHARCODE[0] === Currencies.EURO).VALUE[0]
          );
        }
      })
    );
  }

  convertStrToNum(str: string): number {
    return +str.replace(',', '.');
  }
}
