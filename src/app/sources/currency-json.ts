import { Observable, of } from 'rxjs';
import { Currencies } from './sources';
import { catchError, map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { CurrencyParrent } from '../interfaces/currency-parrent';
import { CurrencyDailyJson } from '../interfaces/currency-daily-json';


export class CurrencyJSON implements CurrencyParrent {
  constructor(
    private url: string,
    private http: HttpClient
  ) {}

  getCourse(): Observable<number> {
    return this.http.get<CurrencyDailyJson>(this.url).pipe(
      map((res: CurrencyDailyJson) => res.Valute[Currencies.EURO].Value),
      catchError((error) => {
        console.log('error', error);
        return of(null);
      })
    );
  }
}
