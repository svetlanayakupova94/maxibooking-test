import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { Component } from '@angular/core';
import { AppState } from '../store/interfaces/app-state';
import { loadCourse } from '../store/actions/course.actions';
import { courseFeatureKey } from '../store/reducers/course.reducer';


@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.scss']
})
export class CourseComponent {
  course$: Observable <number>;
  error$: Observable <string>;

  constructor(
    private store: Store<AppState>
  ) {
    this.fetchDataByStore();
    this.setSubs();
   }

   private setSubs(): void {
    this.course$ = this.store.select(courseFeatureKey, 'value');
    this.error$ = this.store.select(courseFeatureKey, 'error');
   }

   private fetchDataByStore(): void {
      this.store.dispatch(loadCourse());
      setInterval(() => this.store.dispatch(loadCourse()), 10000);
   }
}
