import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { CurrencyFactory, SourceList } from '../sources/currency-factory';


@Injectable({
  providedIn: 'root'
})
export class CourseService {

  constructor(private http: HttpClient) {}

  public async getCourseData(): Promise<number | {message: string}> {
    if (!environment.sources || !Array.isArray(environment.sources)) {
      return { message: 'Отсутствуют источники данных!' };
    }

    const currencyFactory: CurrencyFactory = new CurrencyFactory();

    for (const source of environment.sources) {
      if ( !source[Object.keys(source)[0]] ) {
        continue;
      }

      const currentSource = currencyFactory.create(
        source[Object.keys(source)[0]],
        Object.keys(source)[0] as SourceList,
        this.http
      );
      if (!currentSource) {
        continue;
      }

      const currentCurrency: number = await currentSource.getCourse().toPromise();
      if (currentCurrency) {
        return currentCurrency;
      }
    }

    return { message: 'Доступных источников нет!' };
  }
}
