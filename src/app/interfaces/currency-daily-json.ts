export interface CurrencyDailyJson {
  Valute?: {
    [key: string]: {
      ID?: string;
      Name?: string;
      Value?: number;
      CharCode?: string;
    }
  };
}
