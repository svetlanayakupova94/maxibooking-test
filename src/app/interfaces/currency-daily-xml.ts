export interface CurrencyDailyXML {
  VALCURS: {
    VALUTE?: Valute[];
  };
}

export interface Valute {
  ID?: string[];
  NAME?: string[];
  VALUE?: string[];
  CHARCODE?: string[];
}
