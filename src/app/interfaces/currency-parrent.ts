import { Observable } from 'rxjs';


export interface CurrencyParrent {
  getCourse(): Observable<number>;
}
