export const environment = {
  production: true,
  sources: [
    { cbr_xml: 'https://www.cbr-xml-daily.ru/daily_utf8.xml'},
    { cbr_json: 'https://www.cbr-xml-daily.ru/daily_json.js'},
  ]
};
